# libGDX游戏开发案例

> libGDX是一个基于OpenGL (ES)的跨平台Java游戏开发框架,适用于Windows、Linux、macOS、Android、您的浏览器和iOS。

官网：https://libgdx.com/

## 打包成exe参考：

1. https://blog.csdn.net/weixin_44678104/article/details/101015065
2. https://libgdx.com/wiki/deployment/bundling-a-jre
3. 使用exe4j软件

