package ink.tsg.mario.screen;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.ai.GdxLogger;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.ScreenUtils;

import ink.tsg.mario.Mario;

public class MainMenuScreen implements Screen {

    final Mario game;

    private OrthographicCamera camera;
    private Texture beginImage;

    public MainMenuScreen(final Mario game) {
        this.game = game;
        camera = new OrthographicCamera();
        beginImage = new Texture(Gdx.files.internal("begin.png"));
        camera.setToOrtho(false, 800, 480);
    }


    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        ScreenUtils.clear(0, 0, 0.2f, 1);

        camera.update();
        game.batch.setProjectionMatrix(camera.combined);

        game.batch.begin();
//        game.font.draw(game.batch, "Welcome!!! ", 100, 150);
//        game.font.draw(game.batch, "Tap anywhere to begin!", 100, 100);
        game.batch.draw(beginImage, 350, 250);
        game.batch.end();
        if (Gdx.input.isTouched()) {
            game.setScreen(new GameScreen(game,1000,800));
            Gdx.app.log("MainMenuScreen","change screen");
            dispose();
        }
    }

    // 每次调整窗口大小都会变化
    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
