package ink.tsg.mario.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.ScreenUtils;

import java.io.Console;

import ink.tsg.mario.Mario;

public class GameScreen  implements Screen {

    final Mario game;
    private  World world;
    private  Box2DDebugRenderer renderer;
    private Camera camera;

    // 加载资源
    public GameScreen(final Mario game,int width, int height) {
        this.game = game;
        // 进入这个场景的窗口大小
//        Gdx.graphics.setWindowedMode(width,height);
        // 相机
        camera = new OrthographicCamera();
        // 创建一个重力为10的世界
        world = new World(new Vector2(0, -10f), true);
        // 渲染器
        renderer = new Box2DDebugRenderer();
        renderStatic();
        renderDyna();



    }


    @Override
    public void show() {
        Gdx.app.log("GameScreen"," GameScreen show");
    }

    @Override
    public void render(float delta) {
        // 循环的末尾
        world.step(1/60f, 6, 2);
        ScreenUtils.clear(0, 0, 0.2f, 1);


        renderer.render(world, camera.combined);


    }


    @Override
    public void resize(int width, int height) {
        Gdx.app.log("GameScreen"," GameScreen resize width:"+width+" height:"+height);
    }

    @Override
    public void pause() {
        Gdx.app.log("GameScreen"," GameScreen pause");
    }

    @Override
    public void resume() {
        Gdx.app.log("GameScreen"," GameScreen resume");
    }

    @Override
    public void hide() {
        Gdx.app.log("GameScreen"," GameScreen hide");
    }

    @Override
    public void dispose() {
        Gdx.app.log("GameScreen"," GameScreen dispose");
    }


    private void renderStatic() {

        // 创建我们的身体定义
        BodyDef groundBodyDef = new BodyDef();
        // 设置它的世界位置
        groundBodyDef.position.set(new Vector2(0, -1));

        // 根据定义创建一个实体并将其添加到世界中
        Body groundBody = world.createBody(groundBodyDef);

        // 创建多边形形状
        PolygonShape groundBox = new PolygonShape();
        // 将多边形形状设置为一个盒子，它是我们视口大小的两倍和 20 高setAsBox 将半宽和半高作为参数
        Gdx.app.log("camera.viewportWidth","camera.viewportWidth: "+camera.viewportWidth);
        groundBox.setAsBox(0.99f, 0.1f);
        // 从我们的多边形形状创建一个夹具并将其添加到我们的地面主体
        groundBody.createFixture(groundBox, 0.1f);
        // 在我们自己之后清理
        groundBox.dispose();
    }


    private void renderDyna() {

        // 首先我们创建一个body定义
        BodyDef bodyDef = new BodyDef();
        // 我们将我们的身体设置为动态，对于像地面这样不动的东西，我们将其设置为静态身体
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        // 设置身体在世界上的起始位置
        bodyDef.position.set(0, 1);

        // 使用身体定义在世界上创建我们的身体
        Body body = world.createBody(bodyDef);

        // 创建一个圆形并将其半径设置为 6
        CircleShape circle = new CircleShape();
        circle.setRadius(0.1f);

        // 创建一个夹具定义以将形状应用于
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = circle;
        fixtureDef.density = 0.5f;
        fixtureDef.friction = 0.5f;
        fixtureDef.restitution = 0.5f; // 让它反弹一点

        // 创建我们的夹具并将其附加到身体上
        Fixture fixture = body.createFixture(fixtureDef);

        // Remember to dispose of any shapes after you're done with them!
        // BodyDef and FixtureDef don't need disposing, but shapes do.
        circle.dispose();

    }
}
